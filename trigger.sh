#!/bin/bash
set -e

_DEBUG=0
_VERBOSITY=0
_strategy="linear"
_NOOP=0
_VMPROVIDER="virtualbox"

readonly ORIGINAL_ARGS="$@"

function noop() {
    echo "[$(date + %Y%m%d%H%M%S)]$*"
}

function error_exit() {
    echo "$1" 1>&2
    print_usage
    exit 1
}

function print_usage() {
    echo "Usage $0 -e <ANSIBLE_ARGS> [-d] [-n] [-v[v[v[v[]v]]]] <action> <bundle-role> <bundle-version> <environment>
    -e  <ANSiBLE_ARGS>: (Optional) arguments to pass to ansible
    -d  enable debug mode
    -n  no operation, will log everthing, it's supposed to do
    -v* vebosity level (experimental feature)
    action: deploy|sanity|smoke|monitoring || start|stop|restart (control actions)
    bundleRole: ansible bundle role
    bundleVersion: ansible bundle version
    environment: local|uat|prd
    vault_p  file is expected, if not needed create an empty one with touch
    " >&2
}
echo "ORIGINAL_ARGS: $ORIGINAL_ARGS"
while getopts "e:dnv" opt; do
    case $opt in 
        n)
            _NOOP=1
            ;;
        e)
            ANSIBLE_ARGS="$ANSIBLE_ARGS -e $OPTARG"
            if [ -z "${OPTARGS##*VAGRANT_PROVIDER=docker}" ]; then
                _VMPROVIDER="docker"
            fi
            ;;
        d)
            _DEBUG=1
            _strategy="degug"
            ;;
        v)
            ((_VERBOSITY=_VERBOSITY+1))
            ;;
        \?)
            error_exit "Invalid option: -OPTARG"
            ;;
    esac
done
echo "\$@: $@"
echo "OPTIND: $OPTIND"

shift $((OPTIND-1))
echo "\$@: $@"

readonly ACTION=$1
readonly bundleRole=$2
readonly bundleVersion=$3
readonly environment=$4

readonly LOGFILE=./log/ansd-$ACTION.$environment.$(date +%Y%m%d%H%M%S).log
mkdir -p ./log
function log() {
    if ([ $environment != 'local' ]); then
        echo "[$(date +%Y%m%d%H%M%s)]$*" >> $LOGFILE
    fi
}
log "start"
log $ORIGINAL_ARGS

#Validation
[[ -z $ACTION ]] && error_exit "Please select a valid action"
[[ -z $bundleRole ]] && error_exit "Please select a bundle role"
[[ -z $bundleVersion ]] && error_exit "Please select a bundle version"
[[ -z $environment ]] && error_exit "Please seelct an environment"

# clever code from http://tldp.org/LDP/abs/html/arrays.html#ARRAYSTROPS
AEnv=( "local" "uat" "prd" )
[[ ${AEnv[@]/$environment/XXXXXXXXXXX} == ${AEnv[@]} ]] && error_exit "Selected environment is not allowed ($environment not valid)"

AAction=( "deploy" "sanity" "smoke" "monitoring" "restart" "start" "stop" )
[[ ${AAction[@]/$ACTION/XXXXXXX} == ${AAction[@]} ]] && error_exit "Selected action is not allowed ($ACTION not vaild)"

[[ $_VERBOSITY -lt 0 ]] && error_exit "allowed only 5 levels of verbosity($_VERBOSITY)"
[[ $_VERBOSITY -gt 5 ]] && error_exit "allowed only 5 levels of verbosity($_VERBOSITY)"

# disable debug strategy in producition deployments
if ([ $_DEBUG = 1 ] && [ $ACTION = "deploy" ] && [ $environment ="prd" ]); then
    error_exit "production deployment does not allow debug mode"
fi

#locking
readonly lockfile=$bundleRole.lock
if ([ $environment != "local" ]); then
    if (set -o noclobberl; echo $$ > $lockfile) 2> /dev/null; then
        log "locked at $lockfile with pid $$"
    else
        error_exit "unable to create lock ($lockfile)"
    fi
fi

log "parameter validation successful"

#cleanup 
trap "rm -rf bundle.$bundleRole.json dependencies.$bundleRole.yml ./roles/$bundleRole $lockfile Vagrantfile group_vars/gbmtvault.*" EXIT

#create dependencies.yml file
cat >dependencies.$bundleRole.yml <<EOL
---
- src: http://vagrant:vagrant@efx-nexus.systems.uk.hsbc:8081/nexus/content/repositories/releases/com/hsbc/gfxtooling/bundle-$bundleRole/$bundleVersion/bundle-$bundleRole-$bundleVersion.tar.gz
  name: $bundleRole 
EOL

log "create dependencies file to load bundle-$bundleRole-$bundleVersion.tar.gz"
cat dependencies.$bundleRole.yml >> "$LOGFILE"

#Force update the dependency
ansible-galaxy install -r dependencies.$bundleRole.yml -p roles -f

# add default param overload if missing
ANSIBLE_ARGS=${ANSIBLE_ARGS:-""}

# create bundle.$bundleRole.json file
cat >bundle.$bundleRole.json <<EOL
{
  "BUNDLE": {
    "ROLE": "$bundleRole",
    "VERSION": "$bundleVersion",
  },
  "ENV": "$environment",
  "ACTION": "$ACTIOn",
  "remote_user": "devtools$environment",
  "TARGET_HOST": "primary",
  "no_log": false,
  "vm_provider": "$_VMPROVIDER",
  "strategy": "_$strategy"
}
EOL

log "bundle variables created"
cat bundle.$bundleRole.json >> "$LOGFILE"

if [ -f roles/$bundleRole/vars/$environment.yml ]; then
    ANSIBLE_ARGS="$ANSIBLE_ARGS -e @roles/$bundleRole/vars/$environment.yml"
fi

# copy app secrets
if [ -f roles/$bundleRole/vars/secret-$environment ]; then
    cp roles/$bundleRole/vars/secret-$environment group_vars/gbmtvault.$bundleRole-$bundleVersion-$environment.vlt
    ANSIBLE_ARTS="$ANSIBLE_ARGS -e @group_vars/gbmvault.$bundleRole-$bundleVersion-$environment.vlt"
fi

if [ -f roles/$bundleRole/vars/secret ]; then
    cp roles/$bundleRole/vars/secret group_vars/gbmtvault.$bundleRole-$bundleVersion.vlt
    ANSIBLE_ARTS="$ANSIBLE_ARGS -e @group_vars/gbmvault.$bundleRole-$bundleVersion.vlt"
fi

# validate existing inventory in the role (TODO: create a dynamic inventory feature)
if ! [ -f roles/$bundleRole/$environment ]; then
    error_exit "Inventory not found at: roles/$bundleRole/$environment"
fi

# if action ==test append to command --check
PARAMS="$ANSIBLE_ARGS -i roles/$bundleRole/$environment -e @bundle.$bundleRole.json --vault-password-file=vault_p"
PLAYBOOK="__$ACTION.yml"
COMMAND="ansible-playbook $PARAMS $PLAYBOOK"

[[ $_VERBOSITY -gt -1 ]] || COMMAND="$COMMAND -vvvvv"
[[ $ACTION = "sanity" ]] && COMMAND="$COMMAND --check --diff"

log "running: $COMMAND"

# run
[[ $_NOOP = 1 ]] && $COMMAND && exit 0

if [ "environment" = "local" ]; then
    echo "=================PROVISION======================"
    ansible-playbook -vvvvv -e IsAVagrantRun=False $PARAMS __provision-local.yml
    echo "virtual provider: $_VMPROVIDER"
    if [ "$_VMPROVIDER" = "docker" ]; then
        ANSIBLE_HOST_KEY_CHECKING=false vagrant ssh devmaster -c "sh -c 'source /home/devtoolsuat/.bashrc && bash -x /vagrant/roles/vagrant-local/files/docker_setup.sh && cd /vagrant && bash -x ansible-playbook -e IsAVagrantRun=True $PARAMS -e remote_user=devtoolsuat $PLAYBOOK'"
    else
        ANSIBLE_HOST_KEY_CHECKING=False vagrant ssh devmaster -c "sudo -u efx sh -c 'source ~/.bash_profile && cd /vagrant && bash -x ansible-playbook -e IsAVagrantRun=True $PARAMS -e remote_user=devtoolsuat $PLAYBOOK'"
    fi
else
    echo "RUN>> $COMMAND"
    $COMMAND
fi

